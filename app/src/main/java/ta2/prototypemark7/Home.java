package ta2.prototypemark7;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Home extends AppCompatActivity {

    private static final int PICK_FILE_REQUEST = 1;
    Button upload;
    TextView nameAPK;
    private String selectedFileName;
    private static final String TAG = Home.class.getSimpleName();
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //checking the permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            finish();
            startActivity(intent);
            return;
        }

        upload = (Button) findViewById(R.id.btn_upload);
        nameAPK= (TextView) findViewById(R.id.file_name);

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APKChooser();
            }
        });
    }

    public void APKChooser(){
        Intent i = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 100);
//        Intent intent = new Intent();
//        intent.setType("application/*");
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//        intent.setType("*/*");
        //allows to select data and return it
//        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
//        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICK_FILE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
        //the image URI
//        if (resultCode == Activity.RESULT_OK) {
//            if (requestCode == PICK_FILE_REQUEST) {
//                if (data == null) {
//                    //no data present
//                    return;
//                }

                Uri selectedImage = data.getData();

                selectedFileName = FilePath.getPath(this, selectedImage);

                String fileName = selectedFileName.substring(selectedFileName.lastIndexOf("/") + 1);

                Log.i(TAG, "Selected File Path:" + selectedFileName);

                if (selectedFileName != null && !selectedFileName.equals("")) {
                    nameAPK.setText(fileName);
                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }

                uploadFile(selectedImage, "My APK");
            }
        }

//            String path = selectedImage.getPath();
//            String fileName = path.substring(path.lastIndexOf("/")+1);
//            nameAPK.setText(fileName);

            //calling the upload file method after choosing the file

    private void uploadFile(Uri fileUri, String desc) {

        //creating a file
        File file = new File(getRealPathFromURI(fileUri));

        RequestBody requestFile = RequestBody.create(MediaType.parse(getRealPathFromURI(fileUri)), file);

        //creating request body for file
//        RequestBody requestFile = RequestBody.create(MediaType.parse(getContentResolver().getType(fileUri)), file);
        RequestBody descBody = RequestBody.create(MediaType.parse("text/plain"), desc);

        //The gson builder
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        //creating retrofit object
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        //creating our api
        Api api = retrofit.create(Api.class);

        //creating a call and calling the upload image method
        Call<MyResponse> call = api.uploadImage(requestFile, descBody);

        //finally performing the call
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                if (!response.body().error) {
                    Toast.makeText(getApplicationContext(), "File Berhasil diupload...", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Ada kesalahan dalam proses...", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /*
     * This method is fetching the absolute path of the image file
     * if you want to upload other kind of files like .pdf, .docx
     * you need to make changes on this method only
     * Rest part will be the same
     * */
    private String getRealPathFromURI(Uri contentUri){
//        String[] proj = {MediaStore.Files.FileColumns.DATA};
//        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
//        Cursor cursor = loader.loadInBackground();
//        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA);
//        cursor.moveToFirst();
//        String result = cursor.getString(column_index);
//        cursor.close();
//        return result;

        String path = null;
        String[] proj = { MediaStore.MediaColumns.DATA };

        if("content".equalsIgnoreCase(contentUri.getScheme ()))
        {
            Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                path = cursor.getString(column_index);
            }
            cursor.close();
            return path;
        }
        else if("file".equalsIgnoreCase(contentUri.getScheme()))
        {
            return contentUri.getPath();
        }
        return null;

//        contentUri = MediaStore.Files.getContentUri("external");
//
//        String[] projection = null;
//
//        // exclude media files, they would be here also.
//        String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
//                + MediaStore.Files.FileColumns.MEDIA_TYPE_NONE;
//        String[] selectionArgs = null; // there is no ? in selection so null here
//
//        String sortOrder = null; // unordered
//        Cursor allNonMediaFiles = cr.query(contentUri, projection, selection, selectionArgs, sortOrder);
//        int column_index = allNonMediaFiles.getColumnIndex(selection);
//        allNonMediaFiles.moveToFirst();
//        String result = allNonMediaFiles.getString(column_index);
//        allNonMediaFiles.close();
//        return result;

//        String path = null;
//        String[] proj = { MediaStore.MediaColumns.DATA };
//        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
//        if (cursor.moveToFirst()) {
//            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//            path = cursor.getString(column_index);
//        }
//        cursor.close();
//        return path;
    }
}
